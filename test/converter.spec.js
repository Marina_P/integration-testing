// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex convertions", () => {
        it("converts the baisic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
            expect(redHex).to.equal("#ff0000"); // red hex value

            const greenHex = converter.rgbToHex(0, 255, 0); // #ff0000
            expect(greenHex).to.equal("#00ff00"); // red hex value

            const blueHex = converter.rgbToHex(0, 0, 255); //#0000ff
            expect(blueHex).to.equal("#0000ff"); // blue hex value
        });
    });
    describe("Hex to RGB convertions", () => {
        it("converts the baisic colors", () => {
            const red = converter.hexToRgb("ff0000"); // (255, 0, 0)
            expect(red).to.deep.equal([255, 0, 0]); // red rgb value

            const green = converter.hexToRgb("00ff00"); //(0, 255, 0)
            expect(green).to.deep.equal([0, 255, 0]);

            const blue = converter.hexToRgb("0000ff"); //(0, 0, 255)
            expect(blue).to.deep.equal([0, 0, 255]); // blue rgb value
        });
    });
});