const express = require("express");
const converter = require("./converter");
const app = express();
const PORT = 3000;

// Welcome endpoint
app.get('/', (req, res) => res.send("Welcome!"));

//RGB to Hex endpoint
app.get('/rgbToHex', (req, res) => {
    const red = parseInt(req.query.r, 10);
    const green = parseInt(req.query.g, 10);
    const blue = parseInt(req.query.b, 10);
    const hex = converter.rgbToHex(red, green, blue);
    res.send(hex);
});
app.get('/hexToRgb', (req, res) => {
    const hex = req.query.hex;
    const rgb = converter.hexToRgb(hex);
    res.send(JSON.stringify(rgb));

});

if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(PORT, () => console.log("Server lisening..."));
}